$(function(){ // on dom ready

var cy = cytoscape({
  container: document.getElementById('cy'),
  
  boxSelectionEnabled: false,
  autounselectify: true,
  
  style: [
    {
      selector: 'node',
      css: {
        'content': 'data(id)',
        'text-valign': 'center',
        'text-halign': 'center'
      }
    },
    {
      selector: '$node > node',
      css: {
        'padding-top': '10px',
        'padding-left': '10px',
        'padding-bottom': '10px',
        'padding-right': '10px',
        'text-valign': 'top',
        'text-halign': 'center',
        'background-color': '#bbb'
      }
    },
    {
      selector: 'edge',
      css: {
        'target-arrow-shape': 'triangle',
        'curve-style': 'bezier'
      }
    },
    {
      selector: ':selected',
      css: {
        'background-color': 'black',
        'line-color': 'black',
        'target-arrow-color': 'black',
        'source-arrow-color': 'black'
      }
    }
  ],
  
  elements: {
    nodes: [
      { data: { id: 'Router-A'}, position: { x: 215, y: 85 } },
      { data: { id: 'Router-B'}, position: { x: 315, y: 85 } },
      { data: { id: 'Router-C'}, position: { x: 415, y: 85 } },
      { data: { id: 'Router-D'}, position: { x: 515, y: 85 } },
      { data: { id: 'Router-E'}, position: { x: 615, y: 85 } },

    ],
    edges: [
      { data: { id: 'ab', source: 'Router-A', target: 'Router-B' } },
      { data: { id: 'bc', source: 'Router-B', target: 'Router-C' } },
      { data: { id: 'cd', source: 'Router-C', target: 'Router-D' } },
      { data: { id: 'de', source: 'Router-D', target: 'Router-E' } },
      
    ]
  },
  
  layout: {
    name: 'preset',
    padding: 5
  }
});

}); // on dom ready